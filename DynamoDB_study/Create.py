'''테이블 생성, 데이터 입력'''


import boto3
# boto3 SDK를 불러온다.
#DynamoDB는 NoSQL이라 한 row에 저장할 수 있는 데이터가 400kb뿐. 한번 조회할수 있는 데이터 양도 1MB뿐. 그래서 속도가빠른편


# Get the service resource.
dynamodb = boto3.resource('dynamodb')

# DynamoDB 테이블 생성
table = dynamodb.create_table(
    TableName = 'users',
    KeySchema=[  # DynamoDB의 Primary Key생성을 위하여 Key Schema속성을 사용하고
        {
            'AttributeName' : 'chapter',   # Partition Key
            'KeyType' : 'Hash'
        },
        {
            'AttributeName' : 'verse',   # Sort Key
            'KeyType' : 'Range'
        }
        # 파티션키와 정렬키를 각각 HASH / RANGE 속성값으로 지정합니다.
        # Key는 2개 이상 들어갈 수 없음
        # 파티션키는 테이블에 있는 아이템을 어디다가 나눌지 정할때 쓰임. 무조건 있어야하고 Hash Key라고도 불림. 1개만 있어야함
        # 정렬키는 정렬할때 쓰이고 꼭 필요한건 아님. Range Key라고도 불림
    ],

    AttributeDefinitions = [ # 기본 키에 해당하는 속성의 유형을 AttributeDefinitions로 지정하고
                             # 문자열 유형인 'S'로 지정합니다.
        {
            'AttributeName' : 'chapter',
            'AttributeType' : 'S' # String
        },

        {
          'AttributeName' : 'verse',
          'AttributeType' : 'N' # Number
        },

    ],

    ProvisionedThroughput = { # 읽기 / 쓰기 용량도 함께 지정

        'ReadCapacityUnits' : 10,
        'WriteCapacityUnits' : 10

    }
)

# Wait until the table exists.
table.meta.client.get_waiter('table_exists').wait(Tablename='users')

# Print out some data about table
print(table.item_count)


# 테이블 사용

dynamodb1 = boto3.resource('dynamodb')
table1 = dynamodb1.Table('users') # table변수에다가 dynamodb.Table(테이블명)을 입력

# DynamoDB의 Table에서 사용되는 모든 값은 이 table변수를 통해서 사용
# 모든 파일에 위와 같은 코드가 반드시 들어가야 한다.

'''생성'''

# put_item() : 입력해넣는 것

def insert_a_row(table_name, row):
    dynamodb = boto3.resource('dynamodb',endpoint_url = 'http://localhost:5000')
    table = dynamodb.Table(table_name)
    r = table.put_item(item = row)
    print(r)


example = {
    'chapter' : '김1',
    'verse' : 2,
    'text' : 'bye'
}

# 예시
# insert_a_row('Book', example)

#################################################################################

def insert_a_row1(row):
    dynamodb = boto3.resource('dynamodb',endpoint_url = 'http://localhost:5000')
    table = dynamodb.Table('table01') # 명시적으로 테이블 이름 넣기
    r = table.put_item(item = row)
    print(r)


# NoSQL은 row안에 row를 넣을 수 있음 (RDB는 불가능).넣을수 있는 것 뿐 아니라 여기에 filter도 가능함.
example2 = {
    'date' : '20210509',
    'wcode' : 1,
    'items' : [
        {'id':'1' , 'name':'krk'},
        {'id':'2' , 'name':'kr2'}
    ]
}

# 예시
# insert_a_row1(example2)

