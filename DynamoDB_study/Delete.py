import boto3

# 테이블 삭제
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('users')
table.delete()

# 데이터 row 삭제
def delete_row(table_name,row,q):
    dynamodb = boto3.resource('dynamodb',endpoint_url = 'http://localhost:5000')
    table = dynamodb.Table(table_name)

    try:
        r = table.delete_item(Key=q)
    except ClientError as e:
        print(e.response['Error']['Message'])

    return r['item']