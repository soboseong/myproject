import boto3
from botocore.exceptions import ClientError

# 함수형태
# Dynamodb에 있는 모든 table을 가져오는 코드

def list_table():
    dynamodb = boto3.client('dynamodb', endpoint_url = "http://localhost:5000")
    response = dynamodb.list_tables()
    return response

# dynamodb = boto3.client('dynamodb')
# tables = dynamodb.list_tables()

# get_item()을 이용하여 1개 데이터만 꺼내옴. 같은 key로 데이터 2개 이상 들어갔다면 마지막에 넣은 데이터가 나옴
def read_row(table_name,row,q):
    dynamodb = boto3.resource('dynamodb',endpoint_url = 'http://localhost:5000')
    table = dynamodb.Table(table_name)

    try:
        r = table.get_item(Key=q)
    except ClientError as e:
        print(e.response['Error']['Message'])

    return r['item']

# 예시
# read_row('Book',example,{'shortened_book_nm':'김', 'chapter':1})

# select all
def select_all(table_name):
    dynamodb = boto3.resource('dynamodb', endpoint_url='http://localhost:5000')
    table = dynamodb.Table(table_name)
    r = table.scan()
    return r

