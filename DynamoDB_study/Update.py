import boto3

def update_row(table_name,row,q):
    dynamodb = boto3.resource('dynamodb',endpoint_url = 'http://localhost:5000')
    table = dynamodb.Table(table_name)

    try:
        r = table.update_item(Key=q)
    except ClientError as e:
        print(e.response['Error']['Message'])

    return r['item']