from flask import Flask #flask라는 패키지에서 flask 클래스를 사용하도록 설정
app = Flask(__name__) # create the application instance

#localhost:5000

@app.route("/") #첫번째 페이지를 '/' 기점으로 설정해주는 것(라우팅). 라우팅 후 라우팅한 주소에 대한 함수가 오는것이 flask 규칙
def hello():
    return "<h1>Hello World!</h1>"

@app.route("/about") # 홈페이지주소/about 페이지 들어갈 내용
def about():
    return "<h1>About Page</h1>"

if __name__ == '__main__': #
    app.run(debug=True)