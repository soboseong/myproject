from flask import Flask, render_template , url_for

app = Flask(__name__) # create the application instance

posts = [
    {
        'author' : 'Corey Schafer',
        'title' : 'Blog Post 1',
        'content' : 'First post content',
        'date_posted' : 'April 20, 2018'
    },
    {
        'author' : 'Jane doe',
        'title' : 'Blog Post 2',
        'content' : 'Second post content',
        'date_posted' : 'April 21, 2018'
    }
]

#localhost:5000

@app.route("/") #첫번째 페이지를 '/' 기점으로 설정해주는 것(라우팅). 라우팅 후 라우팅한 주소에 대한 함수가 오는것이 flask 규칙
def hello():
    return render_template('home.html',posts = posts) #위 posts 리스트(오른쪽변수)를 -> 왼쪽 posts로 옮겨줌으로써
                                                      #왼쪽 posts는 home.html상에서 posts변수로 사용할수있게됨

#localhost:5000/about
@app.route("/about") # 홈페이지주소/about 페이지 들어갈 내용
def about():
    return render_template('about.html',title = 'About')

if __name__ == '__main__':
    app.run(debug=True)