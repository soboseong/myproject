import requests
import xmltodict
import lxml
import json

key = "JruKWI5ZJo4JjQMSp1U8dunLBL0%2FggUoZKOdSPVqGrGhi8k3KDdjVK%2F5DfGOFxAdyBvpAqiPo5cJtOoqBAXNyA%3D%3D"
url = "http://apis.data.go.kr/B410004/CoalMine/getCoalMineList?serviceKey={0}".format(key)

content = requests.get(url).content # btyes 형태의 데이터가 content변수에 삽입
print(content)
print(1)
dict = xmltodict.parse(content) #parse()함수를 호출하면 xml -> OrderedDict타입으로 결과 리턴함
print(dict)
print(2)
print(dict['response']['body']['items']) # 최상단에 response 아래 > body 아래 > items 들을 dict형태로 출력
print(3)

#python 에서는 json으로 다루는게 더 편함.
jsonString = json.dumps(dict['response']['body']['items']['item'],ensure_ascii = False) #OrderedDict -> json형태로 변경하여 Dictionary로 변경
#dumps : list -> string형태로 바꾸겠다는 뜻
# OrderedDict 타입에서 Json변화는 json.dumps()함수 사용.
print(jsonString)
print(4)
jsonObj = json.loads(jsonString)
print(jsonObj)
print(5)
for item in jsonObj:
    #print(item)
    print(item['coalName']) #item안에 특정 컬럼을 불러오는 방법.


