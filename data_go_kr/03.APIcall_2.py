import requests, json
from datetime import datetime


def call_api(prd_cd,date):

    req_cnt = '2000'
    key = 'JruKWI5ZJo4JjQMSp1U8dunLBL0%2FggUoZKOdSPVqGrGhi8k3KDdjVK%2F5DfGOFxAdyBvpAqiPo5cJtOoqBAXNyA%3D%3D'
    url = f'http://apis.data.go.kr/B552895/openapi/service/OrgPriceAuctionService/getExactProdPriceList?ServiceKey={key}&pageNo=1&numOfRows={req_cnt}&delngDe={date}&prdlstCd={prd_cd}&_type=json'
    # parameter 서버로 넘겨주는 정보

    res = requests.get(url)
    jo = json.loads(res.content)
    body = jo['response']['body']
    item = body['items']['item']
    print(f' date: {date} len: {len(item)} body : {body["totalCount"]}')
    #print(len(item),body['totalCount'])
    #print(body['totalCount'])

    file_name = f'res_json/{date}_{prd_cd}.json'
    with open(file_name,'w+') as f:
        f.write(json.dumps(item))
        print(file_name,'save success')

# date = '20210302'
# prd_cd = '1202'
# call_api(prd_cd,date)

dates = ['20210302','20210303','20210304','20210305','20210308']
for date in dates:
    print(date, 'start:', datetime.now())  # 시간 얼마나 걸리는지 찍어보기
    call_api('1202',date)
    print(date, 'end:', datetime.now())   #시간 얼마나 걸리는지 찍어보기
    print('-------------------------------------------')