import json, glob
import pandas as pd
import openpyxl #바로 설치창안뜨면 File > setting -> python interpreter 검색 > 추가 > openpyxl > install package

filenames = glob.glob('res_json/*.json')

for filename in filenames:
    df = pd.read_json(filename)
    spl = filename.split('.')
    df.to_excel(f'{spl[0]}.xlsx')
    print(filename.split('.'))